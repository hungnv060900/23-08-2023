package com.devcamp.accountrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.accountrestapi.models.Account;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AccountController {
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        Account account = new Account("1", "Hung", 12000);
        Account account1 = new Account("2", "My", 15000);
        Account account2 = new Account("3", "Ba", 17000);
        ArrayList<Account> listAccounts = new ArrayList<>();
        listAccounts.add( account);
        listAccounts.add( account1);
        listAccounts.add( account2);
        return listAccounts;
    }
}
