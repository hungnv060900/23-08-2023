package com.devcamp.circlerestapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlerestapi.models.Circle;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CircleController {
    @GetMapping("/circle-area")
    public double CircleArea(@RequestParam(required = true,name = "radius") double radius){
        Circle circle = new Circle(radius);
        System.out.println("Circle: ");
        System.out.println(circle.toString());
        System.out.println("Area: ");
        System.out.println(circle.getArea());
        return circle.getArea();
    }
}
