package com.devcamp.stringlengthapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class StringLengthController {
    @GetMapping("/length")
    public int getLength(@RequestParam(required = true,name = "string") String name){
        return name.length();
    }
}
