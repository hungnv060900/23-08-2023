package com.devcamp.splitstringapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class SplitStringController {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true,name="string") String requeString){
        ArrayList<String> splitStrings = new ArrayList<>();
        String[] stringArray = requeString.split(" ");
        for (String string_element : stringArray) {
            splitStrings.add(string_element);
        }
        return splitStrings;
    }
}
