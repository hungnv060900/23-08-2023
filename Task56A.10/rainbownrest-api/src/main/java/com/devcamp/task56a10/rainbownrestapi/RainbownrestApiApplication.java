package com.devcamp.task56a10.rainbownrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbownrestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbownrestApiApplication.class, args);
	}

}
