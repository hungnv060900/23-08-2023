package com.devcamp.invoicerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.invoicerestapi.models.InvoiceItem;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceItemController{
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getListInvoiceItems(){
        InvoiceItem invoiceItem = new InvoiceItem("1", "Hai anh em", 4, 17000);
        InvoiceItem invoiceItem1 = new InvoiceItem("2", "Ba anh em", 5, 18000);
        InvoiceItem invoiceItem2 = new InvoiceItem("3", "Bon anh em", 6, 19000);
        System.out.println(invoiceItem.toString());
        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem2.toString());
        ArrayList<InvoiceItem> listInvoiceItem = new ArrayList<>();
        listInvoiceItem.add( invoiceItem);
        listInvoiceItem.add( invoiceItem1);
        listInvoiceItem.add( invoiceItem2);
        return listInvoiceItem;
    } 
}