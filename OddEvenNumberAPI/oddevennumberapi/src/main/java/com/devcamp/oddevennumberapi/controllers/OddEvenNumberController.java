package com.devcamp.oddevennumberapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class OddEvenNumberController {
    @GetMapping("/checknumber")
    public String checkNumber(@RequestParam(required = true,name = "number")int number){
        if(number%2==0){
            return "It is even number";
        }else
            return "It is odd number";
        
    }
}
