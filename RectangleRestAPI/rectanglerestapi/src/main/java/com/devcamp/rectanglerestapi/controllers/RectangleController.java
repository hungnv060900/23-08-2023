package com.devcamp.rectanglerestapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rectanglerestapi.models.Rectangle;

@RestController
@RequestMapping("/")
public class RectangleController {
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam(required = true, name = "width") float width,@RequestParam(required = true,name = "length") float length){
        Rectangle rectangle = new Rectangle(length, width);
        return rectangle.getArea();
    }
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(@RequestParam(required = true, name = "width") float width,@RequestParam(required = true,name = "length") float length){
        Rectangle rectangle = new Rectangle(length, width);
        return rectangle.getPerimeter();
    }

}
