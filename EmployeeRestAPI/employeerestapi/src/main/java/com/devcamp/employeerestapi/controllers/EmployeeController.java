package com.devcamp.employeerestapi.controllers;

import java.util.ArrayList;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeerestapi.models.Employee;

@RequestMapping("/")
@RestController
@CrossOrigin
public class EmployeeController {
    @GetMapping("/employees")
    public ArrayList<Employee> listEmployee(){

        ArrayList<Employee> employees = new ArrayList<>();
        Employee employee = new Employee(1, "Viet", "Hung", 12000);
        Employee employee1 = new Employee(2, "Tra", "My", 9000);
        Employee employee2 = new Employee(3, "Van", "A", 11300);
        System.out.println(employee.toString());
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        employees.add(employee);
        employees.add(employee1);
        employees.add(employee2);
        
        return employees;
        
    }

}
